import React from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

const root = createRoot(document.getElementById('root') as HTMLElement);
root.render(<h2>Let's play!</h2>);